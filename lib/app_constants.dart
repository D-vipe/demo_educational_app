// Flutter imports:
import 'package:flutter/material.dart';

class AppColors {
  static const Color mainBg = Color.fromRGBO(54, 66, 125, 1);
  static const Color whiteOpacity05 = Color.fromRGBO(255, 255, 255, .5);
  static const Color appPurple = Color.fromRGBO(69, 83, 154, 1);
  static const Color appRed = Color.fromRGBO(255, 75, 107, 1);
  static const Color appYellow = Color.fromRGBO(255, 162, 23, 1);
  static const Color appBlue = Color.fromRGBO(42, 186, 243, 1);
  static const Color appGreen = Color.fromRGBO(100, 192, 72, 1);
  static const Color textBlue = Color.fromRGBO(236, 246, 255, 1);
}
