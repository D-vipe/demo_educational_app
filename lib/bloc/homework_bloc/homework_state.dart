// Package imports:
import 'package:equatable/equatable.dart';

// Project imports:
import 'package:demo_educational_app/models/tasks.dart';

abstract class HomeworkState extends Equatable {
  const HomeworkState();
}

class HomeworkInitialState extends HomeworkState {
  const HomeworkInitialState();
  @override
  List<Object> get props => [];
}

class HomeworkLoadingState extends HomeworkState {
  const HomeworkLoadingState();

  @override
  List<Object?> get props => [];
}

class HomeworkErrorState extends HomeworkState {
  const HomeworkErrorState();

  @override
  List<Object?> get props => [];
}

class HomeworkLoadedState extends HomeworkState {
  final List<UserTasks> homework;
  final String filterType;
  final int allCounter;
  final int dueCounter;
  final int reviewCounter;

  const HomeworkLoadedState(this.homework, this.filterType, this.allCounter,
      this.dueCounter, this.reviewCounter);
  @override
  List<Object> get props => [homework];
}

class HomeworkRefreshedState extends HomeworkState {
  final List<UserTasks> homework;
  final int allCounter;
  final int dueCounter;
  final int reviewCounter;

  const HomeworkRefreshedState(
      this.homework, this.allCounter, this.dueCounter, this.reviewCounter);
  @override
  List<Object> get props => [homework];
}
