// Dart imports:
import 'dart:async';

// Flutter imports:
import 'package:flutter/foundation.dart';

// Package imports:
import 'package:bloc/bloc.dart';

// Project imports:
import 'package:demo_educational_app/repositories/task_repository.dart';
import 'homework_event.dart';
import 'homework_state.dart';

final repository = TasksRepository();

class HomeworkBloc extends Bloc<HomeworkEvent, HomeworkState> {
  @override
  void onTransition(Transition<HomeworkEvent, HomeworkState> transition) {
    if (!kReleaseMode) {
      print({'HomeworkBloc': transition});
    }
    super.onTransition(transition);
  }

  HomeworkBloc() : super(const HomeworkInitialState()) {
    on<HomeworkInitial>((event, emit) => emit(const HomeworkLoadingState()));
    on<HomeworkGet>((event, emit) async {
      emit(const HomeworkLoadingState());
      Map<String, dynamic> repData = await repository.getHomework();

      if (!repData['status']) {
        emit(const HomeworkErrorState());
      } else {
        emit(HomeworkLoadedState(
            repData['data'],
            'all',
            repData['counters']['allTasks'],
            repData['counters']['dueTasks'],
            repData['counters']['reviewTasks']));
      }
    });

    on<HomeworkRefresh>((event, emit) async {
      emit(const HomeworkLoadingState());
      Map<String, dynamic> repData = await repository.getHomework();

      if (!repData['status']) {
        emit(const HomeworkErrorState());
      } else {
        emit(HomeworkLoadedState(
            repData['data'],
            event.filterType,
            repData['counters']['allTasks'],
            repData['counters']['dueTasks'],
            repData['counters']['reviewTasks']));
      }
    });
  }
}
