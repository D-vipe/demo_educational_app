// Package imports:
import 'package:equatable/equatable.dart';

abstract class HomeworkEvent extends Equatable {
  const HomeworkEvent();
}

class HomeworkInitial extends HomeworkEvent {
  const HomeworkInitial();

  @override
  List<Object> get props => [];
}

class HomeworkRefresh extends HomeworkEvent {
  final String filterType;

  const HomeworkRefresh(this.filterType);
  @override
  List<Object> get props => [];
}

class HomeworkGet extends HomeworkEvent {
  const HomeworkGet();

  @override
  List<Object> get props => [];
}
