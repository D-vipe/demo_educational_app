// Project imports:
import 'package:demo_educational_app/models/tasks.dart';
import 'package:demo_educational_app/providers/tasks_provider.dart';

final provider = TasksProvider();

class TasksRepository {
  Future<Map<String, dynamic>> getHomework() async {
    Map<String, dynamic> result = {'status': false, 'error_message': ''};
    List<UserTasks> modifiedData = [];
    int allTasks = 0;
    int dueTasks = 0;
    int reviewTasks = 0;

    // get fake api response with data
    result = await provider.getData();
    if (result['status'] && result['data'] != null) {
      try {
        allTasks = result['data'].length;
        for (var i = 0; i < result['data'].length; i++) {
          if (result['data'][i]['status'] == 'due') {
            dueTasks++;
          }
          if (result['data'][i]['status'] == 'review') {
            reviewTasks++;
          }
          modifiedData.add(UserTasks.fromJson(result['data'][i]));
        }
        result['counters'] = {
          'allTasks': allTasks,
          'dueTasks': dueTasks,
          'reviewTasks': reviewTasks
        };
        result['data'] = modifiedData;
      } catch (_e) {
        result['status'] = false;
        result['error_message'] =
            'Произошла ошибка при обработке данных уроков';
      }
    }

    return result;
  }
}
