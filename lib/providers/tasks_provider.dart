// Project imports:
import 'package:demo_educational_app/sample_data/tasks_data.dart';

class TasksProvider {
  Future<Map<String, dynamic>> getData() async {
    Map<String, dynamic> result = {'status': false, 'error_message': ''};
    List<Map<String, dynamic>>? data;
    // Imitate API request
    try {
      await Future.delayed(const Duration(seconds: 1), () {
        data = tasksData;

        if (data == null) {
          throw Exception('Не удалось получить данные');
        } else {
          result['status'] = true;
          result['data'] = data;
        }
      });
    } on Exception catch (_message) {
      result['error_message'] = _message;
    } catch (_e) {
      print(_e);
      result['error_message'] = 'Что-то пошло не так';
    }

    return result;
  }
}
