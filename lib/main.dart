// Flutter imports:
import 'package:flutter/material.dart';

// Project imports:
import 'package:demo_educational_app/custom_theme.dart';
import 'package:demo_educational_app/fluro_router.dart';

void main() {
  AppRouter.setupRouter();
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Demo platform',
      debugShowCheckedModeBanner: false,
      theme: CustomTheme().appTheme,
      initialRoute: '/',
      onGenerateRoute: AppRouter.router.generator,
      // home: const MyHomePage(title: 'Flutter Demo Home Page'),
    );
  }
}
