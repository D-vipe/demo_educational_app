// Flutter imports:
import 'package:flutter/material.dart';

class TaskScreen extends StatefulWidget {
  final String title;
  final String detail;
  const TaskScreen({Key? key, required this.title, required this.detail})
      : super(key: key);

  @override
  State<TaskScreen> createState() => _TaskScreenState();
}

class _TaskScreenState extends State<TaskScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Выполнение задания'),
      ),
      body: SafeArea(
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 15, vertical: 20),
          child: Column(
            children: [
              Text(widget.title,
                  style: Theme.of(context).textTheme.headline3,
                  textAlign: TextAlign.center),
              const SizedBox(height: 20),
              Text(
                widget.detail,
                style: Theme.of(context).textTheme.bodyText1,
              )
            ],
          ),
        ),
      ),
    );
  }
}
