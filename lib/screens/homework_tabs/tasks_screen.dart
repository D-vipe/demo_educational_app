// Flutter imports:
import 'package:flutter/material.dart';

// Package imports:
import 'package:flutter_bloc/flutter_bloc.dart';

// Project imports:
import 'package:demo_educational_app/bloc/homework_bloc/homework_bloc.dart';
import 'package:demo_educational_app/bloc/homework_bloc/homework_event.dart';
import 'package:demo_educational_app/bloc/homework_bloc/homework_state.dart';
import 'package:demo_educational_app/models/tasks.dart';
import 'package:demo_educational_app/screens/components/error_page.dart';
import 'package:demo_educational_app/screens/components/filter_widget.dart';
import 'package:demo_educational_app/screens/components/loading_indicator.dart';
import 'package:demo_educational_app/screens/components/task_card.dart';

final HomeworkBloc _screenBloc = HomeworkBloc();

class TasksTab extends StatefulWidget {
  const TasksTab({Key? key}) : super(key: key);

  @override
  State<TasksTab> createState() => _TasksTabState();
}

class _TasksTabState extends State<TasksTab>
    with AutomaticKeepAliveClientMixin<TasksTab> {
  @override
  bool get wantKeepAlive => true;

  String activeFilterType = 'all';
  List<UserTasks> tasksList = [];
  List<UserTasks> filteredTasks = [];
  int allTasksCounter = 0;
  int dueTasksCounter = 0;
  int reviewTasksCounter = 0;
  bool filteringItems = false;

  @override
  void initState() {
    super.initState();

    _screenBloc.add(const HomeworkGet());
  }

  void filterTapHandler(String filterType) {
    setState(() {
      if (filterType != activeFilterType) {
        filteringItems = true;
      }
      activeFilterType = filterType;
    });

    if (activeFilterType == 'all') {
      setState(() {
        filteredTasks = tasksList;
        Future.delayed(const Duration(milliseconds: 300),
            () => setState(() => filteringItems = false));
      });
    } else {
      filterTasksList(activeFilterType);
    }
  }

  void filterTasksList(String filterType) async {
    List<UserTasks> _filtered = [];
    if (tasksList.isNotEmpty) {
      for (int i = 0; i < tasksList.length; i++) {
        if (tasksList[i].status == filterType) {
          _filtered.add(tasksList[i]);
        }
      }
    }

    setState(() {
      filteredTasks = _filtered;
      // filteringItems = false;
    });
    Future.delayed(const Duration(milliseconds: 300),
        () => setState(() => filteringItems = false));
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return BlocProvider(
        lazy: false,
        create: (contex) => _screenBloc,
        child: RefreshIndicator(
            onRefresh: () async {
              _screenBloc.add(HomeworkRefresh(activeFilterType));
            },
            child: BlocConsumer<HomeworkBloc, HomeworkState>(
              listener: (context, state) {
                if (state is HomeworkLoadedState) {
                  if (tasksList.isNotEmpty) tasksList.clear();
                  activeFilterType = state.filterType;
                  tasksList = state.homework;
                  allTasksCounter = state.allCounter;
                  dueTasksCounter = state.dueCounter;
                  reviewTasksCounter = state.reviewCounter;

                  if (activeFilterType == 'all') {
                    filteredTasks = tasksList;
                  } else {
                    filterTasksList(activeFilterType);
                  }
                }
              },
              builder: (context, state) {
                if (state is HomeworkInitialState) {
                  // BlocProvider.of<HomeworkBloc>(context)
                  //     .add(const HomeworkGet());
                  return const LoadingIndicator();
                } else if (state is HomeworkLoadingState) {
                  return const LoadingIndicator();
                } else if (state is HomeworkErrorState) {
                  return const ErrorPage();
                } else {
                  return SingleChildScrollView(
                      // width: MediaQuery.of(context).size.width,
                      // margin: const EdgeInsets.only(top: 20),
                      physics: const AlwaysScrollableScrollPhysics(),
                      padding: const EdgeInsets.symmetric(
                          horizontal: 17, vertical: 20),
                      child: SizedBox(
                          height: MediaQuery.of(context).size.height - 220,
                          child: Column(
                            children: [
                              // Row with filter widget
                              Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  FilterCard(
                                    counter: allTasksCounter,
                                    active: activeFilterType == 'all',
                                    type: 'all',
                                    tapHandler: filterTapHandler,
                                  ),
                                  FilterCard(
                                      counter: dueTasksCounter,
                                      active: (activeFilterType == 'all' ||
                                          activeFilterType == 'due'),
                                      type: 'due',
                                      tapHandler: filterTapHandler),
                                  FilterCard(
                                      counter: reviewTasksCounter,
                                      active: (activeFilterType == 'all' ||
                                          activeFilterType == 'review'),
                                      type: 'review',
                                      tapHandler: filterTapHandler),
                                ],
                              ),
                              const SizedBox(height: 17),
                              _buildMainArea(context, state)
                            ],
                          )));
                }
              },
            )));
  }

  Widget _buildMainArea(BuildContext _context, HomeworkState _state) {
    if (filteredTasks.isNotEmpty) {
      return Expanded(
          flex: 4,
          child: ListView.builder(
              itemCount: filteredTasks.length,
              itemBuilder: (BuildContext context, int index) {
                return AnimatedOpacity(
                  duration: filteringItems
                      ? const Duration(milliseconds: 0)
                      : Duration(milliseconds: ((index + 2) * 100)),
                  opacity: filteringItems ? 0 : 1,
                  child: TaskCard(
                    key: UniqueKey(),
                    title: filteredTasks[index].name,
                    taskType: filteredTasks[index].type,
                    daysLeft: filteredTasks[index].daysLeft ?? 0,
                    taskDetail: filteredTasks[index].detail,
                    taskId: filteredTasks[index].id,
                    taskStatus: filteredTasks[index].status,
                  ),
                );
              }));
    } else {
      return Container(
        alignment: Alignment.center,
        child: Text(
          'Новых заданий не найдено',
          style: Theme.of(_context).textTheme.headline2,
        ),
      );
    }
  }
}
