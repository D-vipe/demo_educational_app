// Flutter imports:
import 'package:flutter/material.dart';

class MarksTab extends StatelessWidget {
  const MarksTab({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
        child: Text('Marks', style: Theme.of(context).textTheme.headline1));
  }
}
