// Flutter imports:
import 'package:flutter/material.dart';

// Project imports:
import 'package:demo_educational_app/screens/components/custom_navbar.dart';

class ProfileScreen extends StatefulWidget {
  static const routeName = '/profile';
  const ProfileScreen({Key? key}) : super(key: key);

  @override
  State<ProfileScreen> createState() => _ProfileScreenState();
}

class _ProfileScreenState extends State<ProfileScreen> {
  int bottomNavIndex = 3;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      bottomNavigationBar: const CustomBottomNavbar(
          currentPage: ProfileScreen.routeName,
          curved:
              true), // This trailing comma makes auto-formatting nicer for build methods.
      body: SafeArea(
        child: Center(
            child:
                Text('Profile', style: Theme.of(context).textTheme.headline1)),
      ),
    );
  }
}
