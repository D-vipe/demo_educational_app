// Flutter imports:
import 'package:flutter/material.dart';

// Project imports:
import 'package:demo_educational_app/screens/components/custom_navbar.dart';

class HomeScreen extends StatefulWidget {
  static const routeName = '/';
  const HomeScreen({Key? key}) : super(key: key);

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  int bottomNavIndex = 0;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      bottomNavigationBar: const CustomBottomNavbar(
          currentPage: HomeScreen.routeName,
          curved:
              true), // This trailing comma makes auto-formatting nicer for build methods.
      body: SafeArea(
        child: Center(
            child: Text('Home', style: Theme.of(context).textTheme.headline1)),
      ),
    );
  }
}
