// Flutter imports:
import 'package:flutter/material.dart';

// Project imports:
import 'package:demo_educational_app/screens/components/custom_navbar.dart';

class CalendarScreen extends StatefulWidget {
  static const routeName = '/calendar';
  const CalendarScreen({Key? key}) : super(key: key);

  @override
  State<CalendarScreen> createState() => _CalendarScreenState();
}

class _CalendarScreenState extends State<CalendarScreen> {
  int bottomNavIndex = 1;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      bottomNavigationBar: const CustomBottomNavbar(
          currentPage: CalendarScreen.routeName,
          curved:
              true), // This trailing comma makes auto-formatting nicer for build methods.
      body: SafeArea(
        child: Center(
            child:
                Text('Calendar', style: Theme.of(context).textTheme.headline1)),
      ),
    );
  }
}
