// Flutter imports:
import 'package:flutter/material.dart';

// Package imports:
import 'package:flutter_svg/svg.dart';

// Project imports:
import 'package:demo_educational_app/app_constants.dart';

class FilterCard extends StatefulWidget {
  final int counter;
  final String type;
  final bool active;
  final Function tapHandler;
  const FilterCard(
      {Key? key,
      required this.counter,
      required this.type,
      required this.active,
      required this.tapHandler})
      : super(key: key);

  @override
  State<FilterCard> createState() => _FilterCardState();
}

class _FilterCardState extends State<FilterCard> {
  final Map<dynamic, dynamic> filterTypes = {
    'all': {
      'color': AppColors.appPurple,
      'icon': SvgPicture.asset('assets/images/icons/all_home_work_icon.svg',
          width: 34, height: 33.58)
    },
    'due': {
      'color': AppColors.appRed,
      'icon': SvgPicture.asset('assets/images/icons/due_home_work_icon.svg',
          width: 34, height: 32.58)
    },
    'review': {
      'color': AppColors.appYellow,
      'icon': SvgPicture.asset('assets/images/icons/need_to_review_icon.svg',
          width: 34, height: 34)
    }
  };

  @override
  Widget build(BuildContext context) {
    return InkWell(
      radius: 0,
      onTap: () => widget.tapHandler(widget.type),
      child: Card(
        elevation: 0,
        // margin: const EdgeInsets.symmetric(horizontal: 9),
        color: widget.counter == 0
            ? AppColors.appGreen
            : (widget.active
                ? filterTypes[widget.type]['color']
                : filterTypes[widget.type]['color'].withOpacity(.2)),
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(12),
        ),
        child: Container(
          alignment: Alignment.center,
          padding: const EdgeInsets.fromLTRB(16, 16, 11, 16),
          height: 65,
          width: 96.33,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(
                widget.counter.toString(),
                style: Theme.of(context).textTheme.headline1,
              ),
              filterTypes[widget.type]['icon']
            ],
          ),
        ),
      ),
    );
  }
}
