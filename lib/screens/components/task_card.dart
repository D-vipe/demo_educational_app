// Flutter imports:
import 'package:flutter/material.dart';

// Package imports:
import 'package:expansion_tile_card/expansion_tile_card.dart';
import 'package:flutter_svg/svg.dart';

// Project imports:
import 'package:demo_educational_app/app_constants.dart';
import 'package:demo_educational_app/screens/task_screen.dart';

class TaskCard extends StatefulWidget {
  final int taskId;
  final String title;
  final String taskType;
  final String taskStatus;
  final String taskDetail;
  final int daysLeft;

  const TaskCard(
      {Key? key,
      required this.taskId,
      required this.title,
      required this.taskType,
      required this.taskStatus,
      required this.taskDetail,
      required this.daysLeft})
      : super(key: key);

  @override
  State<TaskCard> createState() => _TaskCardState();
}

class _TaskCardState extends State<TaskCard> {
  final Map<String, dynamic> typeSetting = {
    'video': SvgPicture.asset(
      'assets/images/icons/video_type_icon.svg',
      width: 28.21,
      height: 23,
    ),
    'physical': SvgPicture.asset(
      'assets/images/icons/pe_type_icon.svg',
      width: 32.5,
      height: 28.5,
    ),
    'reading': SvgPicture.asset(
      'assets/images/icons/reading_type_icon.svg',
      width: 29.79,
      height: 25,
    ),
    'creative': SvgPicture.asset(
      'assets/images/icons/creative_type_icon.svg',
      width: 32,
      height: 30.48,
    ),
    'interactive': SvgPicture.asset(
      'assets/images/icons/interactive_type_icon.svg',
      width: 30.21,
      height: 23,
    )
  };

  final Map<String, Color> colorSetting = {
    'active': AppColors.appPurple,
    'due': AppColors.appRed,
    'review': AppColors.appYellow
  };

  final Map<String, Widget> trailingSetting = {
    'due': SvgPicture.asset('assets/images/icons/due_home_work_icon.svg',
        width: 34, height: 31.17),
    'review': SvgPicture.asset('assets/images/icons/need_to_review_icon.svg',
        width: 34, height: 34)
  };

  String inclineDaysWord(int counter) {
    String word = 'день';
    List<String> keyValues = ['2', '3', '4'];
    List splitCounter = counter.toString().split('');

    if (counter == 0) {
      word = 'дней';
    }

    if (counter > 1 && counter <= 20) {
      if (keyValues.contains(counter.toString())) {
        word = 'дня';
      } else {
        word = 'дней';
      }
    } else if (counter > 20 && counter <= 99) {
      if (counter > 21 && keyValues.contains(splitCounter[1])) {
        word = 'дня';
      } else {
        word = 'дней';
      }
    } else if (counter >= 100) {
      //Проверяем два последних символа в числе и затем запускаем функцию рекурсивно
      if (splitCounter[(splitCounter.length - 2)] == '0') {
        return inclineDaysWord(
            int.parse(splitCounter[(splitCounter.length - 1)]));
      } else {
        String cutVal =
            '${splitCounter[(splitCounter.length - 2)]}${splitCounter[(splitCounter.length - 1)]}';
        return inclineDaysWord(int.parse(cutVal));
      }
    }

    return word;
  }

  @override
  Widget build(BuildContext context) {
    return Container(
        padding: const EdgeInsets.only(bottom: 17, left: 5, right: 5),
        child: ExpansionTileCard(
          borderRadius: const BorderRadius.all(Radius.circular(12)),
          contentPadding:
              const EdgeInsets.symmetric(horizontal: 16, vertical: 3.5),
          leading: typeSetting[widget.taskType],
          trailing: widget.taskStatus == 'active'
              ? SizedBox(
                  width: 33,
                  height: 46,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Text(widget.daysLeft.toString(),
                          textScaleFactor: 1,
                          style: Theme.of(context).textTheme.headline1),
                      Text(inclineDaysWord(widget.daysLeft),
                          textScaleFactor: 1,
                          style: Theme.of(context).textTheme.bodyText1)
                    ],
                  ))
              : trailingSetting[widget.taskStatus],
          title: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 30),
            child: Text(
              widget.title,
              style: Theme.of(context).textTheme.bodyText1,
              maxLines: 2,
              overflow: TextOverflow.ellipsis,
            ),
          ),
          baseColor: colorSetting[widget.taskStatus],
          expandedColor: colorSetting[widget.taskStatus],
          children: [
            Container(
              margin: const EdgeInsets.symmetric(vertical: 14, horizontal: 16),
              child: Column(
                children: [
                  Row(
                    children: [
                      Image.asset('assets/images/sample_image.png',
                          width: 78, height: 78, fit: BoxFit.contain),
                      Container(
                        margin: const EdgeInsets.only(left: 16),
                        width: MediaQuery.of(context).size.width - 180,
                        child: Text(
                          'Здесь текст - описание задания. Для современного мира высокотехнологичная концепция общественного уклада однозначно фиксирует необходимость системы массового участия.',
                          softWrap: true,
                          style: Theme.of(context).textTheme.bodyText2,
                        ),
                      )
                    ],
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                  ),
                  const SizedBox(height: 20),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      OutlinedButton(
                          onPressed: () {
                            Navigator.of(context)
                                .push(MaterialPageRoute(builder: (_context) {
                              return TaskScreen(
                                  title: widget.title,
                                  detail: widget.taskDetail);
                            }));
                          },
                          child: Text('Начать',
                              style: Theme.of(context).textTheme.button))
                    ],
                  )
                ],
              ),
            )
          ],
        ));
  }
}
