// Flutter imports:
import 'package:flutter/material.dart';

// Package imports:
import 'package:flutter_svg/flutter_svg.dart';

class CustomBottomNavbar extends StatelessWidget {
  final String currentPage;
  final bool curved;
  const CustomBottomNavbar(
      {Key? key, required this.currentPage, required this.curved})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Material(
      color: Colors.transparent,
      // elevation: 100,
      child: ClipPath(
        child: SizedBox(
          child: Container(
              decoration: const BoxDecoration(
                color: Colors.white,
              ),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: <Widget>[
                  Container(
                    margin: const EdgeInsets.only(bottom: 10),
                    child: IconButton(
                        icon: currentPage == '/'
                            ? SvgPicture.asset(
                                'assets/images/nav_icons/home_icon_selected.svg',
                                width: 24,
                              )
                            : SvgPicture.asset(
                                'assets/images/nav_icons/home_icon.svg',
                                width: 24,
                              ),
                        onPressed: () => currentPage == '/'
                            ? () {}
                            : Navigator.of(context).pushNamed('/')),
                  ),
                  Container(
                    margin: const EdgeInsets.only(bottom: 10),
                    child: IconButton(
                        icon: currentPage == '/calendar'
                            ? SvgPicture.asset(
                                'assets/images/nav_icons/calendar_icon_selected.svg',
                                width: 24,
                              )
                            : SvgPicture.asset(
                                'assets/images/nav_icons/calendar_icon.svg',
                                width: 24,
                              ),
                        onPressed: () => currentPage == '/calendar'
                            ? () {}
                            : Navigator.of(context).pushNamed('/calendar')),
                  ),
                  const SizedBox(
                    width: 80,
                  ),
                  Container(
                    margin: const EdgeInsets.only(bottom: 10),
                    child: IconButton(
                        icon: currentPage == '/homework'
                            ? SvgPicture.asset(
                                'assets/images/nav_icons/homework_icon_selected.svg')
                            : SvgPicture.asset(
                                'assets/images/nav_icons/homework_icon.svg'),
                        onPressed: () => currentPage == '/homework'
                            ? () {}
                            : Navigator.of(context).pushNamed('/homework')),
                  ),
                  Container(
                    margin: const EdgeInsets.only(bottom: 10),
                    child: IconButton(
                        icon: currentPage == '/profile'
                            ? SvgPicture.asset(
                                'assets/images/nav_icons/profile_icon_selected.svg')
                            : SvgPicture.asset(
                                'assets/images/nav_icons/profile_icon.svg'),
                        onPressed: () => currentPage == '/profile'
                            ? () {}
                            : Navigator.of(context).pushNamed('/profile')),
                  ),
                ],
              )),
          height: 64,
          width: double.infinity,
        ),
        clipper: curved ? CurveDraw() : null,
      ),
    );
  }
}

class CurveDraw extends CustomClipper<Path> {
  @override
  getClip(Size size) {
    double sw = size.width;
    double sh = size.height;

    Path path = Path();
    path.moveTo(0, sh);
    path.lineTo(0, 0);
    // path.quadraticBezierTo(0, 0, sh / 2, 0); //1st curve
    path.lineTo(sw / 2 - sw / 5, 0);

    path.cubicTo((sw / 2 - sw / 14), 0, (sw / 2 - sw / 9), (sh / 2), (sw / 2),
        (sh / 1.75));

    path.cubicTo((sw / 2 + sw / 11), (sh / 1.75), (sw / 2 + sw / 11), 0,
        (sw / 2 + sw / 5.5), 0);

    path.lineTo(sw - sh / 2, 0);

    path.quadraticBezierTo(sw, 0, size.width, 0);
    path.lineTo(sw, sh);

    path.close();

    return path;
  }

  @override
  bool shouldReclip(CustomClipper oldClipper) {
    return true;
  }
}
