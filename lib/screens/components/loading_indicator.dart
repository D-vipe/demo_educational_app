// Flutter imports:
import 'package:flutter/material.dart';

// Project imports:
import 'package:demo_educational_app/app_constants.dart';

class LoadingIndicator extends StatelessWidget {
  const LoadingIndicator({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
        height: (MediaQuery.of(context).size.height),
        child: Column(
          children: const [
            Center(
                child: CircularProgressIndicator(
              strokeWidth: 2,
              color: AppColors.appYellow,
            ))
          ],
          mainAxisAlignment: MainAxisAlignment.center,
        ));
  }
}
