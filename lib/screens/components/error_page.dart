// Flutter imports:
import 'package:flutter/material.dart';

// Project imports:
import 'package:demo_educational_app/app_constants.dart';

class ErrorPage extends StatelessWidget {
  const ErrorPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
        height: (MediaQuery.of(context).size.height),
        child: Column(
          children: [
            const Icon(
              Icons.dangerous_outlined,
              color: AppColors.appRed,
              size: 56,
            ),
            const SizedBox(height: 10),
            Center(
                child: Text(
              'Произошла ошибка',
              style: Theme.of(context).textTheme.headline2,
            ))
          ],
          mainAxisAlignment: MainAxisAlignment.center,
        ));
  }
}
