// Flutter imports:
import 'package:flutter/material.dart';

// Project imports:
import 'package:demo_educational_app/screens/components/custom_navbar.dart';
import 'package:demo_educational_app/screens/homework_tabs/marks_screen.dart';
import 'package:demo_educational_app/screens/homework_tabs/tasks_screen.dart';
import 'package:flutter_svg/svg.dart';
import '../app_constants.dart';

class HomeworkScreen extends StatefulWidget {
  static const routeName = '/homework';
  const HomeworkScreen({Key? key}) : super(key: key);

  @override
  State<HomeworkScreen> createState() => _HomeworkScreenState();
}

class _HomeworkScreenState extends State<HomeworkScreen> {
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  PersistentBottomSheetController? _bottomSheetController;
  final List<String> nav = ['Новые задания', 'Мои оценки'];
  bool showButton = false;
  int bottomNavIndex = 2;

  @override
  void initState() {
    showActionButton();
    super.initState();
  }

  void toggleBottomSheet() {
    if (_bottomSheetController == null) {
      _bottomSheetController = _scaffoldKey.currentState?.showBottomSheet(
          (context) => SizedBox(
                height: 300,
                child: Container(
                  decoration: const BoxDecoration(
                      border: Border(
                          top: BorderSide(
                    color: AppColors.whiteOpacity05,
                    width: 2,
                  ))),
                  width: MediaQuery.of(context).size.width,
                  padding:
                      const EdgeInsets.symmetric(horizontal: 15, vertical: 40),
                  child: Column(
                    children: [
                      Row(
                        children: [
                          const Icon(Icons.notifications,
                              color: AppColors.appRed),
                          Text('Здесь могла бы быть важная информация',
                              style: Theme.of(context).textTheme.bodyText1),
                          Container(
                            alignment: Alignment.center,
                            padding: const EdgeInsets.all(5),
                            child: Text('0',
                                style: Theme.of(context).textTheme.headline2),
                          )
                        ],
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      )
                    ],
                  ),
                ),
              ),
          backgroundColor: AppColors.mainBg);
    } else {
      _bottomSheetController?.close();
      _bottomSheetController = null;
    }
  }

  void showActionButton() async {
    Future.delayed(const Duration(milliseconds: 100), () {
      setState(() {
        showButton = true;
      });
    });
  }

  Future<void> hideActionButton() async {
    Future.delayed(const Duration(milliseconds: 100), () {
      setState(() {
        showButton = false;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: nav.length,
      child: Scaffold(
        key: _scaffoldKey,
        appBar: AppBar(
          elevation: 0,
          toolbarHeight: 0,
          automaticallyImplyLeading: false,
          backgroundColor: AppColors.mainBg,
          bottom: TabBar(indicatorColor: Colors.transparent, tabs: [
            Tab(
                child: Container(
              padding: const EdgeInsets.all(0),
              alignment: Alignment.centerRight,
              child: const Text('Новые задания'),
            )),
            Tab(
                child: Container(
              padding: const EdgeInsets.all(0),
              alignment: Alignment.centerLeft,
              child: const Text('Мои оценки'),
            )),
          ]),
        ),
        floatingActionButton: showButton
            ? FloatingActionButton(
                onPressed: toggleBottomSheet,
                backgroundColor: AppColors.appBlue,
                child: SvgPicture.asset(
                    'assets/images/icons/floatingbtnicon.svg',
                    width: 22,
                    height: 22),
              )
            : null,
        floatingActionButtonLocation:
            FloatingActionButtonLocation.miniCenterDocked,
        bottomNavigationBar: const CustomBottomNavbar(
            currentPage: HomeworkScreen.routeName, curved: true),
        body: const TabBarView(
          physics: AlwaysScrollableScrollPhysics(),
          children: [TasksTab(), MarksTab()],
        ),
      ),
    );
  }
}
