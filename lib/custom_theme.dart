// Flutter imports:
import 'package:flutter/material.dart';

// Project imports:
import 'package:demo_educational_app/app_constants.dart';

class CustomTheme {
  ThemeData appTheme = ThemeData(
    scaffoldBackgroundColor: AppColors.mainBg,
    appBarTheme: const AppBarTheme(
        backgroundColor: AppColors.appPurple,
        centerTitle: true,
        titleTextStyle: TextStyle(
            fontSize: 18,
            color: Colors.white,
            fontWeight: FontWeight.w500,
            fontFamily: 'Manrope')),
    bottomAppBarColor: Colors.white,
    // primarySwatch: MaterialColor(0x36427D, CustomTheme().color),
    visualDensity: VisualDensity.adaptivePlatformDensity,
    textTheme: const TextTheme(
      bodyText1: TextStyle(
          color: AppColors.textBlue,
          height: 1,
          fontFamily: 'Manrope',
          fontSize: 14,
          fontWeight: FontWeight.w400),
      bodyText2:
          TextStyle(color: Colors.white, fontFamily: 'Manrope', fontSize: 12),
      headline1: TextStyle(
          color: AppColors.whiteOpacity05,
          height: 1,
          fontFamily: 'Jua',
          fontSize: 30,
          fontWeight: FontWeight.w400),
      headline2: TextStyle(
          color: AppColors.whiteOpacity05,
          fontFamily: 'Manrope',
          fontSize: 18,
          fontWeight: FontWeight.w400),
      headline3: TextStyle(
          color: Colors.white,
          height: 1,
          fontFamily: 'Manrope',
          fontSize: 24,
          fontWeight: FontWeight.w400),
      button: TextStyle(
          fontFamily: 'Manrope',
          fontWeight: FontWeight.w600,
          fontSize: 14,
          color: Colors.white),
      // headline3: TextStyle(color: Colors.white),
      // headline4: TextStyle(color: Colors.white),
    ),
    tabBarTheme: const TabBarTheme(
        labelStyle:
            TextStyle(fontFamily: 'Manrope', fontWeight: FontWeight.w500),
        unselectedLabelStyle: TextStyle(
            fontFamily: 'Manrope',
            fontSize: 16,
            fontWeight: FontWeight.w500,
            color: AppColors.whiteOpacity05)),
    outlinedButtonTheme: OutlinedButtonThemeData(
        style: OutlinedButton.styleFrom(
            padding: const EdgeInsets.symmetric(horizontal: 55, vertical: 15),
            primary: Colors.white.withOpacity(.1),
            backgroundColor: Colors.white.withOpacity(.1),
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(32.0),
            ),
            side: BorderSide(width: 2, color: Colors.white.withOpacity(.5)),
            elevation: 0)),
    bottomNavigationBarTheme:
        const BottomNavigationBarThemeData(backgroundColor: Colors.white),
  );
}
