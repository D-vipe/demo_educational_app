class UserTasks {
  int id;
  String type;
  String status;
  int? daysLeft;
  String name;
  String detail;

  UserTasks(
      {required this.id,
      required this.type,
      required this.status,
      required this.name,
      required this.detail,
      this.daysLeft});

  factory UserTasks.fromJson(Map json) {
    return UserTasks(
        id: json['id'],
        type: json['type'],
        status: json['status'],
        daysLeft: json['days_left'],
        name: json['name'],
        detail: json['detail']);
  }

  Map toJson() {
    return {
      'id': id,
      'type': type,
      'status': status,
      'days_left': daysLeft,
      'name': name,
      'detail': detail
    };
  }
}
