// Package imports:
import 'package:fluro/fluro.dart';

// Project imports:
import 'package:demo_educational_app/screens/calendar_screen.dart';
import 'package:demo_educational_app/screens/home_screen.dart';
import 'package:demo_educational_app/screens/homework_screen.dart';
import 'package:demo_educational_app/screens/profile_screen.dart';

class AppRouter {
  static FluroRouter router = FluroRouter();

  static final Handler _homeHandler = Handler(handlerFunc: (context, args) {
    return const HomeScreen();
  });

  static final Handler _calendarHandler = Handler(handlerFunc: (context, args) {
    return const CalendarScreen();
  });

  static final Handler _homeWorkHandler = Handler(handlerFunc: (context, args) {
    return const HomeworkScreen();
  });

  static final Handler _profileHandler = Handler(handlerFunc: (context, args) {
    return const ProfileScreen();
  });

  static void setupRouter() {
    router.define(HomeScreen.routeName,
        handler: _homeHandler, transitionType: TransitionType.fadeIn);
    router.define(CalendarScreen.routeName,
        handler: _calendarHandler, transitionType: TransitionType.fadeIn);
    router.define(HomeworkScreen.routeName,
        handler: _homeWorkHandler, transitionType: TransitionType.fadeIn);
    router.define(ProfileScreen.routeName,
        handler: _profileHandler, transitionType: TransitionType.fadeIn);
  }
}
